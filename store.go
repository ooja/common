package common

import (
	"gopkg.in/mgo.v2/bson"
)

type Store struct {
    Id bson.ObjectId `bson:"_id,omitempty"`
    Name string
    Domain string
    Categories []map[string]interface{}
    NextCrawlTime interface{}						`bson:"next_crawl_time"`	
    Selectors map[string]interface{}
    IgnoreRobotTXT bool								`bson:"ignore_robot_txt"`
    URLQuery	map[string]map[string]string		`bson:"url_query"`
}