package common

import(
	"time"
	"regexp"
)

// get a key from a map. returns a default value if not found
func GetOrDefault(source map[string]interface{}, key string, df interface{}) interface{} {
	if val, found := source[key]; found {
		return val
	} else {
		return df
	}
} 

// checks if an interface is a reference to an error type
func IIsError(i interface{}) bool {
	if _, ok := i.(error); ok {
		return true
	}
	return false
}

// checks if an interface references an error object
// returns the error is so else nil
func IGetError(i interface{}) error {
	if err, ok := i.(error); ok {
		return err
	} else {
		return nil
	}
}

// Get current time in any time zone
// usage: GetCurrentTimeInZone("Africa/Lagos")
func GetCurrentTimeInZone(loc string) (time.Time, error) {
	t := time.Now()
	utc, err := time.LoadLocation(loc)
	if err != nil {
	    return t, err
	}
	return t.In(utc), nil
}

// checks of a date is a valid RFC 3339 date
func IsValidRFC3339Date(date string) (bool, error) {
	rx := "^([0-9]{4})-([0-9]{2})-([0-9]{2})[TtZz ]{1}([0-9]{2}):([0-9]{2}):([0-9]{2})[0-9.]*([+-0-9]{3}|[Zz]{1})(.)*$"
	match, err := regexp.MatchString(rx, date) 
	return match, err
}