package common

type ListingDoc struct {
    Content string
    ListingURL string
    PageURL string
    CategoryURL string
    StoreId string
    Selectors map[string]interface{}
}